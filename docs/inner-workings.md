# Inner Workings

### Base Url
When running locally, it would be `http://localhost:8080` unless you 
have changed the `-p` flag in docker run. In general, if the command
you ran has `-p x:y`, the port is `x`.

## Working
Proxy is very simple. It is just one endpoint that receives GET, POST,
PUT and DELETE i.e. `/proxy`.

### Request Method
If you want to send a post request, send a POST request to `/proxy`, 
if you want to send a GET request, send a GET request to `/proxy`
and so on.

### Url
To set the url, set the header: `x-feather-url` to the url you want to
send the request to. It should have query parameters if any

Example -

x-feather-url - `http://localhost:5000/abc/def?a=b&c=d`

### Headers
To send headers, set a header in the request with the prefix:
`x-feather-header-`.

Ex -

To send `Content-Type: application/json`, send the following header
to /proxy: `X-Feather-Header-Content-Type: application/json`

We need this prefix because browsers do not allow JS to set some
headers. Read more here: https://developer.mozilla.org/en-US/docs/Glossary/Forbidden_header_name

## Notes:
1. Header names are case-insensitive as per HTTP spec

2. GET and DELETE requests cannot have bodies as per HTTP spec

3. If we receive multiple headers with same name in request or 
response, we will join the values using `,`. In HTTP, only the 
`Set-Cookie` header is allowed to appear more than once. Other 
headers if they have multiple values should be treated as if 
they were comma separated. `Set-Cookie` is returned as 
`x-feather-header-set-cookie-0`, `x-feather-header-set-cookie-1`
and so on.

## Example

If you want to send the following request:

API - `http://localhost:5000/abc/def?a=b&c=d`

Headers:
- `Content-Type: application/json`
- `Authorization: Bearer abcdef`

Send the following request:

API - `http://localhost:8080/proxy`

Headers:
- `X-Feather-Url: http://localhost:5000/abc/def?a=b&c=d`
- `X-Feather-Header-Content-Type: application/json`
- `X-Feather-Header-Authorization: Bearer abcdef`

## Response
Response body and status will be identical to the response from proxy
except if there is a bug in feather itself. Response headers are returned
as is prefixed with x-feather-header i.e. if response returns a header:
`Content-Type: application/json`, proxy returns the header:
`X-Feather-Header-Content-Type: application/json`. This is to prevent
a server setting cookies in the name of feather.

### Exception
Set-Cookie is returned as `X-Feather-Header-Set-Cookie-A` where A is one
or more digits. This is because, Set-Cookie is allowed to appear more than
once. Other headers by default will be joined using comma if they appear
multiple times. So, we return a different number for each Set-Cookie header
so libraries don't join them. 

## What about errors in Feather itself?
If there is an error in feather itself, you will get a response body
like the following:
```json
{
  "feather_error": "Human readable error",
  "message": "Exact exception if any"
}
```

`message` may or may not exist. `feather_error` is guaranteed to exist
if Feather errors out.

Status code will be set appropriately.