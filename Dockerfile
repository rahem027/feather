FROM rahem027/hemil-cowj:0.0.4

COPY . .

ENV PORT=8080

ENTRYPOINT ["java", "--add-opens", "java.base/jdk.internal.loader=ALL-UNNAMED", "--add-opens", "java.base/java.util.stream=ALL-UNNAMED", "-cp" ,"deps/*", "cowj.App", "feather.yaml"]