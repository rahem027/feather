"use strict";

/**
 * HTTP Verb/Request Method
 * @readonly
 * @enum {string}
 */
const Verb = {
    Get: 'GET',
    Post: 'POST',
    Put: 'PUT',
    Delete: 'DELETE',
};

const verbsWithBody = [Verb.Post, Verb.Put];

const verb = {
    /** @type {Verb} */
    current: Verb.Get,
    /**
     * Set the verb for a request
     * @param {Verb} newVerb
     */
    onChange: function (newVerb) {
        document.querySelector('#verb .dropdown-button span').innerText = newVerb;
        this.current = newVerb;
        requestReadOnly.onChange(!verbsWithBody.includes(this.current));
    }
}

const requestReadOnly = {
    current: true,
    onChange: function (newReadOnly) {
        this.current = newReadOnly;

        const body = document.getElementById('body');
        if (newReadOnly) {
            body.setAttribute('readonly', 'readonly');
            requestBody.onChange('');
        } else {
            body.removeAttribute('readonly');
        }
    }
}

const requestBody = {
    current: function () { return document.getElementById('body').value },
    onChange: function (newText) {
        document.getElementById('body').value = newText;
    }
}

const requestPart = {
    body: 'body',
    headers: 'headers',
    queryParams: 'queryParams',
}

const requestPartValues = [requestPart.body, requestPart.headers, requestPart.queryParams];

const updateRequestPart = {
    current: requestPart.body,
    onChange: function (newValue) {
        this.current = newValue;
        const visible = document.getElementById(requestPart[newValue]);
        visible.classList.remove('invisible');

        const invisible = [...requestPartValues];
        invisible
            .filter((value) => value !== newValue)
            .forEach((value) => {
                document.getElementById(value).classList.add('invisible');
            });
    }
}

const oneSecond = 1000;
const oneMinute = 60 * oneSecond;

function humanReadableTime(time) {
    if (time >= oneMinute) {
        return `${(time / oneMinute).toFixed(0)} m ${((time % oneMinute) / oneSecond ).toFixed(2)} s`;
    }
    if (time >= oneSecond) {
        return `${(time / oneSecond).toFixed(2)} s`;
    }

    return `${time} ms`;
}

console.assert(humanReadableTime(100) === '100 ms');
console.assert(humanReadableTime(1000) === '1.00 s');
console.assert(humanReadableTime(1234) === '1.23 s');
console.assert(humanReadableTime(1000 * 60) === '1 m 0.00 s');
console.assert(humanReadableTime(1000 * 60 + 1234) === '1 m 1.23 s');

const oneKiloByte = 1000;
const oneMegaByte = 1000 * oneKiloByte;
const oneGigaByte = 1000 * oneMegaByte;

function humanReadableSize(size) {
    if (size >= oneGigaByte) {
        return `${(size / oneGigaByte).toFixed(2)} KB`;
    }

    if (size >= oneMegaByte) {
        return `${(size / oneMegaByte).toFixed(2)} MB`;
    }

    if (size >= oneKiloByte) {
        return `${(size / oneKiloByte).toFixed(2)} KB`;
    }

    return `${size} B`;
}

console.assert(humanReadableSize(100) === '100 B');
console.assert(humanReadableSize(1000) === '1.00 KB');
console.assert(humanReadableSize(1234) === '1.23 KB');
console.assert(humanReadableSize(1000000) === '1.00 MB');
console.assert(humanReadableSize(1234000) === '1.23 MB');

async function send(e) {
    const form = document.getElementById("sendForm");
    if (!form.checkValidity()) {
        console.log("invalid");
        return;
    }

    e.preventDefault();

    const url = document.getElementById('url').value;
    const body = requestBody.current();

    let options = { method: verb.current };
    if (verbsWithBody.includes(verb.current)) {
        options.body = body;
    }

    const headers = new Headers();
    for (let i = 0; i < headerInputs.rows; i++) {
        const key = headerInputs.inputs.get([i, 0].join(',')).value.trim();
        const value = headerInputs.inputs.get([i, 1].join(',')).value.trim();

        if (key !== '') {
            headers.append(`X-Feather-Header-${key}`, value);
        }
    }

    headers.delete('X-FEATHER-URL');
    headers.append('X-FEATHER-URL', url);

    options.headers = headers;

    try {
        progressIndicator.show();
        const start = Date.now();
        const response = await fetch('/proxy', options);
        const end = Date.now();

        const body = await response.text();
        console.log(`status: ${response.status}`);
        console.log(`body: ${body}`);

        document.getElementById('status').innerText = `${response.status.toString()} ${response.statusText}`;
        document.getElementById('responseTime').innerText = humanReadableTime(end - start);
        document.getElementById('responseSize').innerText = humanReadableSize(body.length);
        document.getElementById('resp').value = body;

        /// set response headers
        const headers = document.getElementById('responseHeaders');

        /**
         * Creating a copy. headers.children is live. If we don't create a copy,
         * it will not delete all elements in the below for loop as deleting
         * elements will change the size of the HTMLCollection.
         *
         * Reference - https://developer.mozilla.org/en-US/docs/Web/API/HTMLCollection
         *
         * "An HTMLCollection in the HTML DOM is live; it is automatically updated when
         * the underlying document is changed. For this reason it is a good idea to make
         * a copy (e.g., using Array.from) to iterate over if adding, moving, or removing nodes."
         */
        const elements = Array.from(headers.children);

        /// First two elements are headers. Ignore it.
        for (let i = 2; i < elements.length; i++) {
            elements[i].remove();
        }

        /// Only headers starting with this prefix are useful.
        /// This prefix is useful because headers like Set-Cookie
        /// if returned by the proxy, can set a cookie on feather
        /// which can lead to security vulnerabilities
        const prefix = 'x-feather-header-';
        const respHeaders = Array.from(response.headers).filter(([key, _]) => key.startsWith('x-feather-header-'));

        console.log(respHeaders);

        for (const [key, value] of respHeaders) {
            const fragment = document.createDocumentFragment();

            const input1 = document.createElement('span');
            input1.classList.add('control');
            input1.classList.add('cell');
            input1.classList.add('no-margin');
            input1.classList.add('overflow');
            input1.classList.add('w50');
            fragment.appendChild(input1);

            const input2 = document.createElement('span');
            input2.classList.add('control');
            input2.classList.add('cell');
            input2.classList.add('no-margin');
            input2.classList.add('overflow');
            input2.classList.add('w50');
            fragment.appendChild(input2);

            /**
             * Set-Cookie is a special header. Other headers are required by
             * the standard to be treated as if they were comma separated if
             * they appear multiple times. Set-Cookie is the only exception.
             * So Set-Cookie headers are returned as `x-feather-set-cookie-0`
             * `x-feather-set-cookie-1` and so on. So, if after removing the prefix,
             * if header starts with set-cookie, then header name is set cookie
             * thereby discarding the number. Otherwise, we just strip the prefix
             */
            const header = key.substring(prefix.length);

            input1.innerText = header.startsWith('set-cookie') ? 'set-cookie' : header;
            input2.innerText = value;

            headers.appendChild(fragment);
        }

        document.getElementById('responseHeaderCount').innerText = `(${respHeaders.length})`;
    } catch (e) {
        console.log(e);
    } finally {
        progressIndicator.hide();
    }
}

/**
 * @typedef Point
 * @type {object}
 * @property {number} x - Zero based value of x
 * @property {number} y - Zero based value of y
 */

/**
 * @typedef EditableTable
 * @type {object}
 * @property {number} rows - Number of rows in the table
 * @property {Map<HTMLInputElement, Point>} locations - Map of input field ==> Point object
 * @property {Map<String, HTMLInputElement>} inputs - Map of x, y co ordinates joined as `${x},${y}` ==> element.
 * Unfortunately, JS does not support list as keys properly. Hence, the hack
 * @property {string} id - ID of the element that refers to EditableTable
 * @property {string} countID - ID of the element that displays the count of rows in table
 */


/**
 * @type EditableTable
 */
const headerInputs = {
    rows: 1,
    /// mapping of input element to {row:x, col:y}
    locations: new Map(),
    /// mapping of [row, col] to input
    inputs: new Map(),
    id: 'headers',
    countID: 'requestHeaderCount',
}

/**
 * @type EditableTable
 */
const queryInputs = {
    rows: 1,
    /// mapping of input element to {row:x, col:y}
    locations: new Map(),
    /// mapping of [row, col] to input
    inputs: new Map(),
    id: 'queryParams',
    countID: 'queryParamCount',
}

function createInputRow(fragment) {
    const div = document.createElement('div');
    div.classList.add('horizontal-flex');

    const input1 = document.createElement('input');
    input1.classList.add('control');
    input1.classList.add('cell');
    input1.classList.add('no-margin');
    div.appendChild(input1);

    const input2 = document.createElement('input');
    input2.classList.add('control');
    input2.classList.add('cell');
    input2.classList.add('no-margin');
    div.appendChild(input2);

    fragment.appendChild(div);
    return [input1, input2];
}

/**
 * Adds a new row if this function see fits
 * @param {EditableTable} inputs - a header input like object which maintains the inputs and their locations
 * @param {KeyboardEvent} event - an event for keydown
 */
function addNewRowIfLast(inputs, event) {
    const location = inputs.locations.get(event.target);
    if (location === undefined) {
        return;
    }

    if (event.shiftKey) {
        return;
    }

    /// if I am on the last row, add a new one
    if (location.row === inputs.rows - 1) {
        const element = document.getElementById(inputs.id);
        const fragment = document.createDocumentFragment();
        const [input1, input2] = createInputRow(fragment);
        element.appendChild(fragment);

        inputs.rows += 1;
        inputs.locations.set(input1, {row: location.row + 1, col: 0});
        inputs.locations.set(input2, {row: location.row + 1, col: 1});
        /// JS cant use list as a key because === in list compares if they
        /// refer to same object not if they are equal. So converting to string
        /// Reference - https://stackoverflow.com/a/43592969
        inputs.inputs.set([location.row + 1, 0].join(','), input1);
        inputs.inputs.set([location.row + 1, 1].join(','), input2);

        /// Last row is empty so dont count it
        document.getElementById(inputs.countID).innerText = `(${inputs.rows - 1})`;
    }
}

/**
 * Remove a row if empty
 * @param {EditableTable} inputs - a header input like object which maintains the inputs and their locations
 * @param {FocusEvent} event - an event for keydown
 */
function removeLastRowIfEmpty(inputs, event) {
    const location = inputs.locations.get(event.target);
    if (location === undefined) {
        return;
    }

    const input1 = inputs.inputs.get([location.row, 0].join(','));
    const input2 = inputs.inputs.get([location.row, 1].join(','));

    if (inputs.rows > 1 && input1.value.trim() === '' && input2.value.trim() === '') {
        const headers = document.getElementById(inputs.id);
        const divs = headers.querySelectorAll('div.horizontal-flex');
        /// First row in this list is the header. So, we need to add one
        divs[location.row+1].remove();

        const headerKey = [location.row, 0].join(',');
        const headerValue = [location.row, 1].join(',');

        inputs.inputs.delete(headerKey);
        inputs.inputs.delete(headerValue);
        inputs.locations.delete(input1);
        inputs.locations.delete(input2);

        for (let i = location.row+1; i < inputs.rows; i++) {
            const headerKey = [i, 0].join(',');
            const input1 = inputs.inputs.get(headerKey);
            inputs.inputs.delete(headerKey);
            inputs.inputs.set([i-1, 0].join(','), input1);
            inputs.locations.set(input1, { row: i-1, col: 0 });

            const headerValue = [i, 1].join(',');
            const input2 = inputs.inputs.get(headerValue);
            inputs.inputs.delete(headerValue);
            inputs.inputs.set([i-1, 1].join(','), input2);
            inputs.locations.set(input2, { row: i-1, col: 1 });
        }

        inputs.rows -= 1;

        /// Last row is empty so dont count it
        document.getElementById(inputs.countID).innerText = `(${inputs.rows - 1})`;
    }
}

const showResponseBody = {
    current: true,
    onChange: function (newValue) {
        this.current = newValue;
        const headers = document.getElementById('responseHeaders');
        const body = document.getElementById('resp');
        if (newValue) {
            headers.classList.add('invisible');
            body.classList.remove('invisible');
        } else {
            headers.classList.remove('invisible');
            body.classList.add('invisible');
        }
    }
};

const url = {
    current: null,
    onChange: function (newValue, updateQueryParamTable = true) {
        try {
            this.current = new URL(newValue);

            if (updateQueryParamTable) { updateQueryParameters(); }
        } catch (e) {
            if (e instanceof TypeError && e.message.startsWith('URL constructor')) { return; }
            throw e;
        }
    },
};

/**
 * Resize table to have target rows. If table has more rows than target,
 * delete them. If table has less rows, add row with empty inputs
 * @param {EditableTable} table
 * @param {number} target
 */
function resize(table, target) {
    /**
     * We want to ensure there are parameter count + 1 rows at the end
     * of this function. One row extra for to add a new query param if any
     * At this point, there are 3 possibilities
     * 1. table.rows < queryParameterCount + 1 - add ((queryParameterCount + 1) - table.row) rows
     * 2. table.rows == queryParameterCount + 1 - do nothing
     * 2. table.rows > queryParameterCount - Remove (table.rows - (queryParameterCount + 1)) rows
     */

    if (table.rows < target) {
        const element = document.getElementById(table.id);
        const fragment = document.createDocumentFragment();

        for (let i = table.rows; i < target; i++) {
            const [input1, input2] = createInputRow(fragment);
            element.appendChild(fragment);

            table.locations.set(input1, {row: i, col: 0});
            table.locations.set(input2, {row: i, col: 1});

            table.inputs.set([i, 0].join(','), input1);
            table.inputs.set([i, 1].join(','), input2);
        }
    } else if (table.rows > target) {
        /// index start from 0. Hence the (- 1)
        for (let i = table.rows - 1; i >= target; i--) {
            const keyString = [i, 0].join(',');
            const key = table.inputs.get(keyString);
            key.remove();
            table.inputs.delete(keyString);
            table.locations.delete(key);

            const valueString = [i, 1].join(',');
            const value = table.inputs.get(valueString);
            value.remove();
            table.locations.delete(valueString);
            table.locations.delete(value);
        }
    }

    /// at this point, rows should be target
    table.rows = target;
    /// Last row is empty so dont count it
    document.getElementById(table.countID).innerText = `(${table.rows - 1})`;
}

function updateQueryParameters() {
    /// copying into array to allow index based lookup
    const searchParams = Array.from(url.current.searchParams);
    const queryParamCount = searchParams.length;

    resize(queryInputs, queryParamCount + 1);

    for (let i = 0; i < queryParamCount; i++) {
        const key = queryInputs.inputs.get([i, 0].join(','));
        key.value = searchParams[i][0];

        const value = queryInputs.inputs.get([i, 1].join(','));
        value.value = searchParams[i][1];
    }

    /// Make sure last row is empty
    queryInputs.inputs.get([queryParamCount, 0].join(',')).value = '';
    queryInputs.inputs.get([queryParamCount, 1].join(',')).value = '';
}

function indexOr(string, char, defaultValue) {
    const index = string.indexOf(char);
    return index === -1 ? defaultValue : index;
}

function updateUrl() {
    const urlElement = document.getElementById('url');

    const queryParams = new URLSearchParams();
    for (let i = 0; i < queryInputs.rows; i++) {
        const key = queryInputs.inputs.get([i, 0].join(','));
        if (key.value !== '') { queryParams.append(key.value, queryInputs.inputs.get([i, 1].join(',')).value); }
    }

    const index = indexOr(urlElement.value, '?', urlElement.value.length);
    const newUrl = urlElement.value.substring(0, index) + '?' + queryParams;
    url.onChange(newUrl, false);
    urlElement.value = newUrl;
}


/**
 * @typedef Request
 * @type {object}
 * @property {Verb} verb - Request method
 * @property {string} url - Url for the request. Includes query parameters.
 * url is a string not a URL object as initially we want url to be empty but
 * new URL('') throws an exception that the URL is invalid
 * @property {Map<string, string>} headers - Headers of the request
 * @property {string} body - Only respected if verbsWithBody contains verb
 */

/**
 * Update UI to show the request given in the parameter
 * @param {Request} request
 */
function show(request) {
    document.getElementById('verb').value = request.verb;
    document.getElementById('url').value = request.url;

    const bodyAllowed = verbsWithBody.includes(request.verb);
    console.log(`Body allowed: ${bodyAllowed}`);

    requestReadOnly.onChange(!bodyAllowed);

    document.getElementById('body').value = bodyAllowed ? request.body : '';

    document.getElementById('resp').value = '';

    resize(headerInputs, request.headers.size + 1);

    const headers = Array.from(request.headers.keys());

    for (let i = 0; i < request.headers.size; i++) {
        headerInputs.inputs.get([i, 0].join(',')).value = headers[i];
        headerInputs.inputs.get([i, 1].join(',')).value = request.headers.get(headers[i]);
    }

    headerInputs.inputs.get([request.headers.size, 0].join(',')).value = '';
    headerInputs.inputs.get([request.headers.size, 1].join(',')).value = '';

    let queryParams = [];
    try {
        queryParams = Array.from(new URL(request.url).searchParams.entries());
    } catch (e) {
        console.log('not a valid url. Using empty query parameters')
    }

    resize(queryInputs,  queryParams.length + 1);

    for (let i = 0; i < queryParams.length; i++) {
        queryInputs.inputs.get([i, 0].join(',')).value = queryParams[i][0];
        queryInputs.inputs.get([i, 1].join(',')).value = queryParams[i][1];
    }

    queryInputs.inputs.get([queryParams.length, 0].join(',')).value = '';
    queryInputs.inputs.get([queryParams.length, 1].join(',')).value = '';

    document.getElementById('resp').value = '';
}

const progressIndicator = {
    showing: false,
    show: function () {
        this.showing = true;
        document.getElementById('progressIndicator').classList.remove('invisible');
    },
    hide: function() {
        this.showing = false;
        document.getElementById('progressIndicator').classList.add('invisible');
    }
}

const dropdownItemSelected = {
    // Need to use arrow function otherwise, this is incorrect inside onChange
    'verb': (newVerb) => verb.onChange(newVerb)
}

function setUpDropDowns() {
    for (let dropdown of document.querySelectorAll('.dropdown')) {
        const button = dropdown.querySelector('.dropdown-button');

        const menu = dropdown.querySelector('.dropdown-menu');

        /// on button click, show drop down menu
        button.addEventListener('click', () => {
            menu.classList.remove('hidden');
            menu.classList.add('dropdown-menu-visible');
        });

        for (let item of dropdown.querySelectorAll('.dropdown-item')) {
            item.addEventListener('click', () => {
                menu.classList.add('hidden');
                menu.classList.remove('dropdown-menu-visible');

                dropdownItemSelected[dropdown.id](item.dataset.value);
            });
        }
    }
}

const tabBarUpdated = {
    'requestPart': (part) => {
        const input = {
            body: requestPart.body,
            headers: requestPart.headers,
            query: requestPart.queryParams,
        };
        updateRequestPart.onChange(input[part]);
    },
    'responsePart': (part) => {
        if (part === 'body') {
            showResponseBody.onChange(true);
        } else {
            showResponseBody.onChange(false);
        }
    }
}

function setUpTabBars() {
    for (let tabBar of document.querySelectorAll('.tab-bar')) {
        const tabBarItems = tabBar.querySelectorAll('.tab-item');
        for (let item of tabBarItems) {
            item.addEventListener('click', () => {
                for (let item of tabBarItems) { item.classList.remove('tab-item-selected'); }

                item.classList.add('tab-item-selected');

                tabBarUpdated[tabBar.id](item.dataset.value);
            });
        }
    }
}

function updateTableMaxHeight() {
    /**
     * Set table height. The body section should use maximum space available after laying out all the other
     * elements. But it should also scroll if necessary to not distort the layout if it overflows.
     * This is not possible in naive css to my knowledge. After asking CSS expert and friend Rohan, he suggested
     * I use javascript to set the maximum height.
     */

    /**
     * Logic is this:
     * 1. Get the height of element[1].
     * 2. Set max height of all the tables.
     *
     * But it is slightly more complicated. Height of invisible elements is always 0[2]. So, we temporarily
     * store the original state, show the response body, get its size and then change it back to the original
     * state.
     *
     * Note: In my testing, I never see the body being rendered.
     *
     * References -
     * 1. https://stackoverflow.com/questions/294250/how-do-i-retrieve-an-html-elements-actual-width-and-height
     * 2. https://stackoverflow.com/questions/1473584/need-to-find-height-of-hidden-div-on-page-set-to-displaynone
     */
    const visible = showResponseBody.current;

    showResponseBody.onChange(true);
    const resp = document.getElementById('resp');


    const height = resp.offsetHeight;

    showResponseBody.onChange(visible);

    /// Sometimes the element is not visible.
    if (height <= 0) { return; }

    for (const table of document.querySelectorAll('.table')) {
        table.style.maxHeight = `${height}px`;
    }
}


window.onload = () => {
    const form = document.getElementById("sendForm");
    form.onsubmit = send;

    /*const verbElement = document.getElementById("verb");
    verbElement.onchange = (e) => verb.onChange(e.target.value);

    verb.onChange(verbElement.selectedOptions[0].value);*/

    const urlElement = document.getElementById('url');
    urlElement.addEventListener('keyup', (event) => url.onChange(event.target.value));

    /// initialize headerInputs
    const inputs = document.querySelectorAll('#headers input');
    inputs.forEach(function (input, index) {
        headerInputs.locations.set(input, { row: 0, col: index });
        headerInputs.inputs.set([0, index].join(','), input);
    });

    console.log(headerInputs);

    const headers = document.getElementById('headers');
    headers.addEventListener('focusout', (event) => removeLastRowIfEmpty(headerInputs, event));
    headers.addEventListener('keydown', (event) => addNewRowIfLast(headerInputs, event));

    /// initialize queryParams
    const queryParamInputs = document.querySelectorAll('#queryParams input');
    queryParamInputs.forEach(function (input, index) {
        queryInputs.locations.set(input, { row: 0, col: index });
        queryInputs.inputs.set([0, index].join(','), input);
    });

    console.log(queryParamInputs);

    const queryParams = document.getElementById('queryParams');
    queryParams.addEventListener('focusout', (event) => { removeLastRowIfEmpty(queryInputs, event); updateUrl(); });
    queryParams.addEventListener('keydown', (event) => { addNewRowIfLast(queryInputs, event); });
    queryParams.addEventListener('keyup', updateUrl);

    /// Firefox seems to remember the values of some inputs across refresh.
    /// Reset all inputs to their blank state
    show({ verb: Verb.Get, url: '', headers: new Map(), body: '' });

    setUpDropDowns();
    setUpTabBars();
    updateTableMaxHeight();
}

window.addEventListener('resize', updateTableMaxHeight);
